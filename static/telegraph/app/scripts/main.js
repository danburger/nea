$(document).ready(function() {

  // resizeFrame
  // --------------------------
  jQuery.event.add(window, "load", resizeFrame);
  jQuery.event.add(window, "resize", resizeFrame);

  function resizeFrame(){
      var h = $(document).height();
      $(".portal-control-panel").css('height',(h));
  }


  // +/- Form Field Blocks
  // ---------------------------
  var max_fields      = 10; //maximum input boxes allowed
  var wrapper         = $(".add-remove-form-fields#lines-annotations"); //Fields wrapper
  var add_button      = $(".add_field_button"); //Add button ID

  var x = 1; //initlal text box count
  $(add_button).click(function(e){ //on add input button click
      e.preventDefault();
      if(x < max_fields){ //max input box allowed
          x++; //text box increment
          $(wrapper).append('<div class="toggle-fields" id="toggle-lines-annotations"><hr><div class="clearfix"><div class="col-lg-12"><div class="form-group"><div class="col-lg-3"><label for="" class="control-label">Type</label></div><div class="col-lg-9"><select class="form-control"><option>Text</option><option>Line</option><option>Arrow</option><option>Function</option><option>Treadline</option></select></div></div></div></div><div class="clearfix"><div class="col-lg-12"><div class="form-group"><div class="col-lg-3"><label for="" class="control-label">Position</label></div><div class="col-lg-9"><div class="col-lg-6"><input type="text" class="form-control" id="" placeholder="X"></div><div class="col-lg-6"><input type="text" class="form-control" id="" placeholder="Y"></div></div></div></div></div><div class="clearfix"><div class="col-lg-12"><div class="form-group"><div class="col-lg-3"><label for="" class="control-label">Text</label></div><div class="col-lg-9"><input type="text" class="form-control" id="" placeholder="10"></div></div></div></div><div class="clearfix"><div class="col-lg-12"><div class="form-group"><div class="col-lg-3"><label for="" class="control-label">Size</label></div><div class="col-lg-9"><input type="text" class="form-control" id="" placeholder="10"></div></div></div></div><div class="clearfix"><div class="col-lg-12"><div class="form-group"><div class="col-lg-3"><label for="" class="control-label">Color</label></div><div class="col-lg-9"><input type="text" class="colorpicker form-control" value="#fff" id="" ></div></div></div></div><div class="clearfix"><div class="col-lg-12"><div class="form-group"><div class="col-lg-3"><label for="" class="control-label">Angle</label></div><div class="col-lg-9"><input type="text" class="form-control" id="" placeholder="10"></div></div></div></div><br><div class="clearfix"><a href="#" class="btn btn-default right btn-xs remove_field">Remove Fields</a></div></div>'); //add input box
      }
  });

  $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parents('div.toggle-fields').remove(); x--;
  })


  // Toggle All Checkboxes
  // ----------------------------
  $('.toggle-all-checkboxes').click (function () {
       var checkedStatus = this.checked;
      $('.toggle-all-checkboxes-wrap').find(':checkbox').each(function () {
          $(this).prop('checked', checkedStatus);
       });
  });


  // Toggle Checkboxes - Controls
  // ----------------------------

    $('ul#parent-tabs.nav.nav-tabs li a').click(function(){
      var toggle_checks = $(this).attr("aria-controls");
      var toggle_wrap   = "aside.portal-control-panel"
      $(this).parents(toggle_wrap).attr("class", "portal-control-panel col-lg-3 " + toggle_checks +"-checks-show");

    });



  // Tooltips
  // ---------------------------
  $(function () {
    $('[data-toggle="tooltip"]').tooltip({container: 'body'})
  });

  // Popover
  // ---------------------------
  $(function () {
    $('[data-toggle="popover"]').popover()
  })


  // Tabs
  // ---------------------------
  $('#tabs a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
  });

  // Show More
  // ---------------------------
  $('#toggle-more-controls').click(function (e) {
    $('.more-controls.collapse').toggleClass('fade').slideToggle('fast');
    $('#toggle-more-controls').html($('#toggle-more-controls').text() == 'Less Options' ? 'More Options' : 'Less Options');
  })


  // Filter More
  // ---------------------------
  $('#toggle-filters').click(function (e) {
    $('#toggle-filters').html($('#toggle-filters').text() == 'Hide Filters' ? 'Show Filters' : 'Hide Filters');
  })

  // Gradient Animation
  // ---------------------------
  // 66,181,116  // green
  // 174,206,57  // lightgreen
  // 121,204,242 // blue
  // 144,131,188 // purple
  // 122,133,158 // light midnight

  var colors = new Array(
  [144,131,188],
  [121,204,242],
  [144,131,188],
  [121,204,242],
  [122,133,158],
  [144,131,188]
  );

  var step = 0;
  //color table indices for:
  // current color left
  // next color left
  // current color right
  // next color right
  var colorIndices = [0,1,2,3];

  //transition speed
  var gradientSpeed = 0.002;

  function updateGradient()
  {

    if ( $===undefined ) return;

  var c0_0 = colors[colorIndices[0]];
  var c0_1 = colors[colorIndices[1]];
  var c1_0 = colors[colorIndices[2]];
  var c1_1 = colors[colorIndices[3]];

  var istep = 1 - step;
  var r1 = Math.round(istep * c0_0[0] + step * c0_1[0]);
  var g1 = Math.round(istep * c0_0[1] + step * c0_1[1]);
  var b1 = Math.round(istep * c0_0[2] + step * c0_1[2]);
  var color1 = "rgb("+r1+","+g1+","+b1+")";

  var r2 = Math.round(istep * c1_0[0] + step * c1_1[0]);
  var g2 = Math.round(istep * c1_0[1] + step * c1_1[1]);
  var b2 = Math.round(istep * c1_0[2] + step * c1_1[2]);
  var color2 = "rgb("+r2+","+g2+","+b2+")";

   $('.welcome-panel').css({
     background: "-webkit-gradient(linear, left top, right top, from("+color1+"), to("+color2+"))"}).css({
     background: "-moz-linear-gradient(left, "+color1+" 0%, "+color2+" 100%)"});

    step += gradientSpeed;
    if ( step >= 1 )
    {
      step %= 1;
      colorIndices[0] = colorIndices[1];
      colorIndices[2] = colorIndices[3];

      //pick two new target color indices
      //do not pick the same as the current one
      colorIndices[1] = ( colorIndices[1] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
      colorIndices[3] = ( colorIndices[3] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;

    }
  }
  setInterval(updateGradient,10);


});