# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

import numpy as np
import pandas as pd
import os
import json
import base64
import zlib
import datetime
from bokeh.plotting import figure, show, output_file, ColumnDataSource
from bokeh.models import HoverTool, ColumnDataSource, ColorBar, LinearColorMapper, LogColorMapper, \
    CategoricalColorMapper, CategoricalMarkerMapper, CustomJS, Title
from bokeh.embed import components
from bokeh.io import export_png, export_svgs, curdoc
from bokeh.themes import built_in_themes
from math import log, exp
from bokeh.core.properties import value
from gluon.storage import Storage
import random

def clear():
    settings.forget()
    return "Done"

def get_all_variables():
    settings = Storage()
    settings.mode = request.vars.mode or "scatter"
    settings.xaxis = request.vars.xaxis or "ra"
    if settings.mode == "scatter":
        settings.yaxis = request.vars.yaxis or "dec"
    else:
        settings.yaxis = request.vars.yaxis or "none"
    settings.color = request.vars.color or "none"
    settings.legend = request.vars.legend or "bottom right"
    settings.size = request.vars.size or "none"
    settings.alpha = request.vars.alpha or "none"
    settings.xaxislog = request.vars.xaxislog or None
    settings.xaxisinv = request.vars.xaxisinv or None
    settings.xaxismin = request.vars.xaxismin or None
    settings.xaxismax = request.vars.xaxismax or None
    settings.xaxiscumul = request.vars.xaxiscumul or None
    settings.yaxislog = request.vars.yaxislog or None
    settings.yaxisinv = request.vars.yaxisinv or None
    settings.yaxismin = request.vars.yaxismin or None
    settings.yaxismax = request.vars.yaxismax or None
    settings.colorlog = request.vars.colorlog or None
    settings.colorinv = request.vars.colorinv or None
    settings.sizelog = request.vars.sizelog or None
    settings.sizeinv = request.vars.sizeinv or None
    settings.alphalog = request.vars.alphalog or None
    settings.alphainv = request.vars.alphainv or None
    settings.titlelabel = request.vars.titlelabel or None
    settings.titlelabelsize = request.vars.titlelabelsize or "20"
    settings.xaxislabel = request.vars.xaxislabel or None
    settings.xaxislabelsize = request.vars.xaxislabelsize or "12"
    settings.yaxislabel = request.vars.yaxislabel or None
    settings.legendlabel = request.vars.legendlabel or None
    settings.legendlabelsize = request.vars.legendlabelsize or "12"
    settings.function = request.vars.function or None
    settings.discrete_palette = request.vars.discrete_palette or "Default"
    settings.continuous_palette = request.vars.continuous_palette or "Viridis"
    settings.ticksize = request.vars.ticksize or "12"
    settings.width = request.vars.width or "750"
    settings.height = request.vars.height or "625"
    settings.darkmode = request.vars.darkmode or None
    settings.ncolumns = request.vars.ncolumns or "30"
    settings.nlegend = request.vars.nlegend or "10"
    return settings

# ---- example index page ----
def index():
    a = np.load(NEA_DATA_FILE)
    if len(request.args) > 0 and not request.vars:
        savestate_fname = os.path.join(FG_SAVESHARE_FOLDER,request.args(0))
        with open(savestate_fname) as f:
            settings = json.loads(f.read())['settings']
        settings = Storage(settings)
        return locals()
    settings = get_all_variables()
    return locals()

def graph():
    settings = get_all_variables()
    for var in settings.keys():
        if settings[var] == None:
            del(settings[var])

    #Save the state
    #http://stackoverflow.com/questions/4659524/how-to-sort-by-length-of-string-followed-by-alphabetical-order
    lowbound = 1000000L
    security_code = str(random.randint(lowbound,lowbound*10-1))
    while os.path.exists(os.path.join(FG_SAVESHARE_FOLDER,security_code)):
        lowbound *= 10
        security_code = str(random.randint(lowbound,lowbound*10-1))
    with open(os.path.join(FG_SAVESHARE_FOLDER,security_code),'w') as f:
        f.write(json.dumps(dict(
            url=security_code,
            portal_name='nea',
            settings=settings
        )))
    url = request.env.http_host + "/" + security_code
    if request.is_https:
        url = "https://" + url

    a = np.load(NEA_DATA_FILE)

    #get last update
    last_update = np.sort(a['rowupdate'])[-1]
    last_update_as_date = datetime.datetime.strptime(last_update,"%Y-%m-%d")

    outputs = pd.DataFrame()
    outputs['xaxis'] = a[settings.xaxis]
    if settings.mode == "scatter" or settings.yaxis != 'none':
        outputs['yaxis'] = a[settings.yaxis]
    if settings.color != "none":
        outputs['color'] = a[settings.color]
    if settings.size != "none":
        outputs['size'] = a[settings.size]
    if settings.alpha != "none":
        outputs['alpha'] = a[settings.alpha]
    outputs['host'] = a['hostname']
    outputs['letter'] = a['pl_letter']

    outputs = outputs.replace([np.inf, -np.inf], np.nan)
    outputs = outputs[~outputs.isna().any(1)]
    if settings.xaxislog:
        outputs = outputs[outputs['xaxis']>0]
    if settings.yaxislog:
        outputs = outputs[outputs['yaxis']>0]
    if settings.colorlog:
        outputs = outputs[outputs['color']>0]
    if settings.sizelog:
        outputs = outputs[outputs['size']>0]
    if settings.alphalog:
        outputs = outputs[outputs['alpha']>0]

    if settings.size != "none":
        sizedata = outputs['size']
        if settings.sizelog:
            sizedata = np.log(sizedata)
        sizedata = (sizedata - min(sizedata)) / (max(sizedata)-min(sizedata))
        if settings.sizeinv:
            sizedata = 1 - sizedata
        sizedata = sizedata * 12 + 4
        outputs['sizescale'] = sizedata
        size = "sizescale"
    else:
        size = 6

    if settings.alpha != "none":
        alphadata = outputs['alpha']
        if settings.alphalog:
            alphadata = np.log(alphadata)
        alphadata = (alphadata - min(alphadata)) / (max(alphadata)-min(alphadata))
        if settings.alphainv:
            alphadata = 1 - alphadata
        outputs['alphascale'] = alphadata
        alpha = "alphascale"
    else:
        alpha = 1
    
    #build the plot
    TOOLS="crosshair,pan,wheel_zoom,zoom_in,zoom_out,box_zoom,undo,redo,reset,tap,save,box_select,poly_select,lasso_select,"

    if settings.darkmode:
        curdoc().theme = "dark_minimal"
    else:
        curdoc().theme = "caliber"    

    #all timing variations are lumped together
    pd.set_option('mode.chained_assignment',None)
    for axis in outputs: 
        if outputs[axis].dtype.kind == "O":
            outputs[axis][outputs[axis] == 'Pulsation Timing Variations'] = 'Timing Variations'
            outputs[axis][outputs[axis] == 'Transit Timing Variations'] = 'Timing Variations'
            outputs[axis][outputs[axis] == 'Eclipse Timing Variations'] = 'Timing Variations'

    #build the labels
    labels = {}
    unique_labels = {}
    for axis in outputs:
        if axis not in ["sizescale",'alphascale','host','letter']:
            labels[axis] = settings.get(axis)
            if labels[axis] not in unique_labels.values():
                unique_labels[axis] = labels[axis]

    #set the axis type: log or linear
    x_axis_type=(settings.xaxislog and "log" or "linear")
    y_axis_type=(settings.yaxislog and "log" or "linear")
    
    #if there is no data then return a message
    if len(outputs['xaxis']) == 0:
        return "No data points to display"
        
    #histogram
    if settings.mode == "hist" and settings.yaxis != "none":
        if x_axis_type == "log":
            outputs['xaxis'] = np.log10(outputs['xaxis'])
        if y_axis_type == "log":
            outputs['yaxis'] = np.log10(outputs['yaxis'])
        if settings.xaxismin:
            outputs = outputs[outputs['xaxis']>=float(settings.xaxismin)]
        if settings.xaxismax:
            outputs = outputs[outputs['xaxis']<=float(settings.xaxismax)]
        if settings.yaxismin:
            outputs = outputs[outputs['yaxis']>=float(settings.yaxismin)]
        if settings.yaxismax:
            outputs = outputs[outputs['yaxis']<=float(settings.yaxismax)]
        x_items, x_labels = segments(outputs['xaxis'],x_axis_type=x_axis_type,eccentricity=(settings.xaxis=="pl_orbeccen"),target=int(settings.ncolumns))
        y_items, y_labels = segments(outputs['yaxis'],x_axis_type=y_axis_type,eccentricity=(settings.yaxis=="pl_orbeccen"),target=int(settings.nlegend))
        if len(y_items) > int(settings.nlegend or "10"):
            return "Too many items on Y-axis"
        if len(y_items) > 10:
            return "Too many items on Y-axis"
        raw_data = hist2d(x_items,y_items,outputs['xaxis'],outputs['yaxis'])
        xy_data = {'_columns_':x_labels}
        for i in range(len(y_labels)):
            if settings.xaxiscumul:
                raw_data[i] = [sum(raw_data[i][:j+1]) for j in range(len(raw_data[i]))]
            xy_data[y_labels[i]] = raw_data[i]
        p = figure(x_range=x_labels, width=int(float(settings.width)), height=int(float(settings.height)), 
            tools=TOOLS, tooltips="$name, @_columns_: @$name planets")
        p.vbar_stack(y_labels, x='_columns_', width=0.9, line_width=0,
            fill_color=discrete_palettes[settings.discrete_palette][:len(y_labels)], source=xy_data,
            legend=[value(y) for y in y_labels])
        p.y_range.start = 0
        y_data = hist1d(x_items,outputs['xaxis'])
        if settings.xaxiscumul:
            y_data = [sum(y_data[:i+1]) for i in range(len(y_data))]
        p.y_range.end = max(y_data)
        p.xaxis.major_label_orientation = 0.5
        p.legend.location = settings.legend.replace(" ","_")
        p.legend.title = settings.legendlabel or get_display_string(labels['yaxis'])
        p.legend.title_text_font_size = settings.legendlabelsize + "pt"
        p.legend.label_text_font_size = settings.ticksize + "pt"
        if settings.darkmode:
            p.legend.title_text_color = "#e0e0e0"
        if settings.darkmode:
            p.legend.border_line_color = "#adafb0"
            p.legend.border_line_alpha = 100
            p.legend.background_fill_color = "#20262b"
            p.legend.background_fill_alpha = 100
        else:
            p.legend.border_line_color = "#5b5b5b"
            p.legend.border_line_alpha = 100
            p.legend.background_fill_color = "white"
            p.legend.background_fill_alpha = 100
    elif settings.mode == "hist":
        if x_axis_type == "log":
            outputs['xaxis'] = np.log10(outputs['xaxis'])
        if settings.xaxismin:
            outputs = outputs[outputs['xaxis']>=float(settings.xaxismin)]
        if settings.xaxismax:
            outputs = outputs[outputs['xaxis']<=float(settings.xaxismax)]
        x_items, x_labels = segments(outputs['xaxis'],x_axis_type=x_axis_type,eccentricity=(settings.xaxis=="pl_orbeccen"),target=int(settings.ncolumns))
        y_data = hist1d(x_items,outputs['xaxis'])
        if settings.xaxiscumul:
            y_data = [sum(y_data[:i+1]) for i in range(len(y_data))]
        source = ColumnDataSource(data=dict(x_labels=x_labels, y_data=y_data))
        p = figure(x_range=x_labels, width=int(float(settings.width)), height=int(float(settings.height)), tools=TOOLS)
        p.vbar(x='x_labels', top='y_data', width=0.9, source=source,
            line_color='white', fill_color='green')
        p.y_range.start = 0
        p.y_range.end = max(y_data)
        p.xaxis.major_label_orientation = 0.5
    else: #Scatter
        #handle tooltips
        source = ColumnDataSource(outputs)
        tooltips = "<div style='max-width:300px'><b>@host</b><b>@letter</b><br/>" + "<br/>".join([get_display_string(labels[axis])+" = @"+axis + get_units(labels[axis]) for axis in unique_labels]) + "</div>"
        hover = HoverTool(tooltips=tooltips)
        #build the bounds
        if outputs['xaxis'].dtype.kind == "O":
            x_range = np.unique(outputs["xaxis"])
        else:
            x_range = [min(outputs['xaxis']),max(outputs['xaxis'])]
        if outputs['yaxis'].dtype.kind == "O":
            y_range = np.unique(outputs["yaxis"])
        else:
            y_range = [min(outputs['yaxis']),max(outputs['yaxis'])]
        #add bounds
        if settings.xaxismin:
            x_range[0] = float(settings.xaxismin)
        if settings.xaxismax:
            x_range[1] = float(settings.xaxismax)
        if settings.yaxismin:
            y_range[0] = float(settings.yaxismin)
        if settings.yaxismax:
            y_range[1] = float(settings.yaxismax)
        #invert the axis if necessary
        if settings.xaxisinv:
            x_range = x_range[::-1]
        if settings.yaxisinv:
            y_range = y_range[::-1]
        #build the figure
        p = figure(tools=[TOOLS,hover], width=int(float(settings.width)), height=int(float(settings.height)), 
            x_range=x_range, 
            y_range=y_range,
            x_axis_type=x_axis_type,y_axis_type=y_axis_type)
        p.outline_line_width = 1
        #p.outline_line_alpha = 0.3
        p.outline_line_color = "black"

        #ticks for strings
        if type(x_range[0]) == str:
            x_ticks = np.arange(0.5,len(x_range)+0.5)
            x_legend = {}
            for tick, item in zip(x_ticks,x_range):
                x_legend[tick] = item
            p.xaxis.ticker = x_ticks
            p.xaxis.major_label_orientation = 0.5
            p.xaxis.major_label_overrides = x_legend
        if type(y_range[0]) == str:
            y_ticks = np.arange(0.5,len(y_range)+0.5)
            y_legend = {}
            for tick, item in zip(y_ticks,y_range):
                y_legend[tick] = item
            p.yaxis.ticker = y_ticks
            p.yaxis.major_label_orientation = 0.5
            p.yaxis.major_label_overrides = y_legend
            
        #color if needed
        if 'color' in outputs and outputs['color'].dtype.kind == "O":
            factors = np.dstack(np.unique(outputs['color'],return_counts=True))[0].tolist()
            factors.sort(key=lambda x: x[1], reverse=True)
            print(repr(factors))
            factors = zip(*factors)[0]
            mapper = CategoricalColorMapper(factors=factors, palette=discrete_palettes[settings.discrete_palette])
            marker_mapper = CategoricalMarkerMapper(factors=factors, default_value="circle", markers=['circle','square','triangle','hex','inverted_triangle']*100)
            p.scatter('xaxis', 'yaxis', fill_color={'field': 'color', 'transform': mapper}, 
                line_color={'field': 'color', 'transform': mapper}, 
                marker={'field': 'color', 'transform': marker_mapper}, 
                source=source, size=size, fill_alpha=alpha, line_alpha=0, legend='color')
            p.legend.location = settings.legend.replace(" ","_")
            p.legend.title = settings.legendlabel or get_display_string(labels['color'])
            p.legend.title_text_font_size = settings.legendlabelsize + "pt"
            p.legend.label_text_font_size = settings.ticksize + "pt"
            if settings.darkmode:
                p.legend.title_text_color = "#e0e0e0"
            if settings.darkmode:
                p.legend.border_line_color = "#adafb0"
                p.legend.border_line_alpha = 100
                p.legend.background_fill_color = "#20262b"
                p.legend.background_fill_alpha = 100
            else:
                p.legend.border_line_color = "#5b5b5b"
                p.legend.border_line_alpha = 100
                p.legend.background_fill_color = "white"
                p.legend.background_fill_alpha = 100
        elif 'color' in outputs:
            palette = continuous_palettes[settings.continuous_palette]               
            #invert the z-axis if necessary
            if settings.colorinv:
                palette = palette[::-1]
            #set up a log scale if necessary
            if settings.colorlog:
                mapper = LogColorMapper(palette=palette, low=min(outputs['color']), high=max(outputs['color']))
            else:
                mapper = LinearColorMapper(palette=palette, low=min(outputs['color']), high=max(outputs['color']))
            #bulid the scatter plot
            p.scatter('xaxis', 'yaxis', fill_color={'field': 'color', 'transform': mapper}, 
                line_color={'field': 'color', 'transform': mapper}, 
                source=source, size=size, fill_alpha=alpha, line_alpha=0)
            color_bar_title = settings.legendlabel or labels['color']
            color_bar = ColorBar(color_mapper=mapper, height=int(settings.ticksize)*10, title=get_display_string(color_bar_title,add_units=True), 
                width=int(len(color_bar_title)*int(settings.legendlabelsize)*0.6), 
                title_text_font_size = str(settings.legendlabelsize) + "px", 
                border_line_width=1, border_line_color='#cccccc', label_standoff=int(int(settings.ticksize)*0.75),
                major_label_text_font_size = str(settings.ticksize) + "px",
                location=settings.legend.replace(" ","_"))
            if settings.colorlog:
                color_bar.major_label_overrides = {
                    0.1: '0.1', 0.01: '0.01', 0.001: '0.001', 0.0001: '0.0001', 0.00001: '0.00001',
                    1: '1', 10: '10', 100: '100', 1000: '1000', 10000: '10000', 100000: '100000'}
            p.add_layout(color_bar)
        else:
            #build the scatter plot
            p.scatter('xaxis', 'yaxis', size=size, fill_alpha=alpha, line_alpha=0, fill_color='#42b574', line_color='#42b574', source=source)
        if settings.function:
            N = 1000
            x_func = np.linspace(min(outputs['xaxis']),max(outputs['xaxis']),N)
            y_func = eval(settings.function,{'x':x_func},{'sin':np.sin,'cos':np.cos,'tan':np.tan,'sqrt':np.sqrt})
            p.line(x=x_func,y=y_func)
        source.callback = CustomJS(code="""
            var inds = cb_obj.selected['1d'].indices;
            var d1 = cb_obj.data;
            var result = [];
            for (i = 0; i < inds.length; i++) {
                result.push(d1['host'][inds[i]]+d1['letter'][inds[i]]);
            }
            $("#selected_planets").val(result.join(", "));
            if(result.length > 0)
            {
                $("#selected_list").show();
            }
            else
            {
                $("#selected_list").hide();
            }
        """)
        
    #miscellaneous settings
    if settings.titlelabel:
        p.title.text = settings.titlelabel
        p.title.text_font_size = settings.titlelabelsize + "px"
        p.title.align = 'center'
    p.xaxis.axis_label = settings.xaxislabel or get_display_string(labels['xaxis'],add_units=True)
    if 'yaxis' in labels:
        p.yaxis.axis_label = settings.yaxislabel or get_display_string(labels['yaxis'],add_units=True)
    else:
        p.yaxis.axis_label = 'Count'
    last_updated = last_update_as_date.strftime("%d %b %Y")
    p.add_layout(Title(text=last_updated + "          exoplanetarchive.ipac.caltech.edu", align="left"), "above")
    p.xaxis.axis_label_text_font_size = settings.xaxislabelsize + "pt"
    p.xaxis.axis_label_standoff = int(settings.xaxislabelsize)
    p.xaxis.axis_label_text_font_style = "normal"
    p.xaxis.major_label_text_font_size = settings.ticksize + "pt"
    p.xaxis.major_label_text_font_style = "normal"
    p.yaxis.axis_label_text_font_size = settings.xaxislabelsize + "pt"
    p.yaxis.axis_label_text_font_style = "normal"
    p.yaxis.axis_label_standoff = int(settings.xaxislabelsize)
    p.yaxis.major_label_text_font_size = settings.ticksize + "pt"
    p.yaxis.major_label_text_font_style = "normal"
    if settings.xaxislog:
        p.xaxis.major_label_overrides = {
            0.1: '0.1', 0.01: '0.01', 0.001: '0.001', 0.0001: '0.0001', 0.00001: '0.00001',
            1: '1', 10: '10', 100: '100', 1000: '1000', 10000: '10000', 100000: '100000'}
    if settings.yaxislog:
        p.yaxis.major_label_overrides = {
            0.1: '0.1', 0.01: '0.01', 0.001: '0.001', 0.0001: '0.0001', 0.00001: '0.00001',
            1: '1', 10: '10', 100: '100', 1000: '1000', 10000: '10000', 100000: '100000'}

    #callbacks
    callback_x = CustomJS(args=dict(xr=p.x_range), code="""
        $("#xaxismin").val(roundToSix(xr.start));
        $("#xaxismax").val(roundToSix(xr.end));
        """)
    p.x_range.js_on_change('start', callback_x)
    callback_y = CustomJS(args=dict(yr=p.y_range), code="""
        $("#yaxismin").val(roundToSix(yr.start));
        $("#yaxismax").val(roundToSix(yr.end));
        """)
    p.y_range.js_on_change('start', callback_y)


    #generate PNG, SVG
    if request.extension == "png":
        return export_png(p)
    elif request.extension == "svg":
        p.output_backend = "svg"
        return export_svgs(p)
        
    #generate HTML
    script, html = components(p)
    
    #send back to the browser
    return dict(script=script,html=html,url=url)
    

def dropdown():
    a = np.load(NEA_DATA_FILE)
    return locals()

# ---- API (example) -----
@auth.requires_login()
def api_get_user_email():
    if not request.env.request_method == 'GET': raise HTTP(403)
    return response.json({'status':'success', 'email':auth.user.email})

# ---- Smart Grid (example) -----
@auth.requires_membership('admin') # can only be accessed by members of admin groupd
def grid():
    response.view = 'generic.html' # use a generic view
    tablename = request.args(0)
    if not tablename in db.tables: raise HTTP(403)
    grid = SQLFORM.smartgrid(db[tablename], args=[tablename], deletable=False, editable=False)
    return dict(grid=grid)

# ---- Embedded wiki (example) ----
def wiki():
    auth.wikimenu() # add the wiki to the menu
    return auth.wiki() 

# ---- Action for login/register/etc (required for auth) -----
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())

# ---- action to server uploaded static content (required) ---
@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)
