from bokeh.palettes import Greys256, Inferno256, Magma256, Plasma256, Viridis256, Cividis256

discrete_palettes = {
    "Default": ["#117733","#ff0000","#7f00ff","#007fff","#ddcc77","#999933","#ababab","#ff7f00","#bbcc33","#545454"],
    "Colorblind": ["#117733","#cc6677","#332288","#882255","#999933","#ddcc77","#009988","#aa4499","#bbcc33","#ababab"],
    "Category10": ["#1f77b4","#ff7f0e","#2ca02c","#d62728","#9467bd","#8c564b","#e377c2","#7f7f7f","#bcbd22","#17becf"],
    "Paired": ["#1f78b4","#33a02c","#e31a1c","#ff7f00","#6a3d9a","#a6cee3","#b2df8a","#fb9a99","#fdbf6f","#cab2d6"],
    "Paul Tol Muted": ["#332288","#88ccee","#44aa99","#117733","#999933","#ddcc77","#cc6677","#882255","#aa4499","#dddddd"],
    "Paul Tol Light": ["#77aadd","#44bb99","#aaaa00","#ee8866","#99ddff","#bbcc33","#eedd88","#ffaabb","#999999","#dddddd"],
    "Paul Tol Discrete": ["#1964B0","#4DB264","#F7F057","#DB060B","#F4A637","#7BB0DF","#CAE1AC","#882D71","#E8601C","#71190D"]
}

discrete_palettes_desc = {
    "Default": "As faithful to the original pre-generated Exoplanet Archive plots as possible",
    "Colorblind": "As faithful to the colorblind pre-generated Exoplanet Archive plots as possible",
    "Category10": "10-category palette based on the Bokeh palette, which in turn is based on D3",
    "Paired": "Palette from Bokeh with five colors and five lighter versions of those colors",
    "Paul Tol Muted": "Muted palette from Paul Tol",
    "Paul Tol Light": "Light palette from Paul Tol",
    "Paul Tol Discrete": "10-category discrete palette from Paul Tol"
}

continuous_palettes = {
    "Viridis":Viridis256,
    "Greys":Greys256,
    "Inferno":Inferno256,
    "Magma":Magma256,
    "Plasma":Plasma256,
    "Cividis":Cividis256,
}