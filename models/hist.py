def segments(data,eccentricity=False, target=30, x_axis_type="linear"):
	if data.dtype.kind == "O":
		data = data.astype(str)
		x_items = data.unique()
		if len(x_items) > target:
			return "Too many items on x-axis"
		x_items.sort()
		#y_data = [len(data[data == x_item]) for x_item in x_items]
		return (x_items,x_items)
	elif min(data) > 1900 and max(data) < 2100:
		low_bound = min(data) 
		high_bound = max(data) 
		x_items = np.arange(low_bound,high_bound)
		x_labels = [str(x_item) for x_item in x_items]
		#y_data = [len(data[data == int(x_item)]) for x_item in x_items]
		return (x_items,x_labels)
	elif eccentricity and x_axis_type == "linear":
		x_items = np.arange(0.0,1.0,0.1)
		#y_data = [len(data[data>=x][data<(x+0.1)]) for x in x_items]
		x_labels = ["%.1f to %.1f" % (x,x+0.1) for x in x_items]
		x_labels[0] = "<" + x_labels[0].split(" to ")[1]
		x_labels[-1] = ">" + x_labels[-1].split(" to ")[0]
		return (x_items,x_labels)
	else:
		x_item_steps = [1,2,5,10,20,50,100,200,500,1000,2000,5000,10000,20000,50000,100000,200000,500000,1000000,2000000,5000000,10000000,20000000,50000000,100000000,200000000,500000000]
		for x_item_step in x_item_steps:
			low_bound = min(data) // x_item_step * x_item_step
			high_bound = max(data) // x_item_step * x_item_step + x_item_step
			if (high_bound - low_bound) / x_item_step > 100:
				continue
			x_items = np.arange(low_bound,high_bound,x_item_step)
			#y_data = [len(data[data>=x][data<(x+x_item_step)]) for x in x_items]
			if x_axis_type == "log":
				x_labels = ["%g to %g" % (10**int(x),10**(int(x)+x_item_step)) for x in x_items]
			else:
				x_labels = [str(int(x)) + " to " + str(int(x)+x_item_step) for x in x_items]
			x_labels[0] = "<" + x_labels[0].split(" to ")[1]
			x_labels[-1] = ">" + x_labels[-1].split(" to ")[0]
			if len(x_items) < target:
				break
		data = data.astype(str)
		return (x_items,x_labels)
	
def hist1d(x_items,data):
	if x_items.dtype.kind == "O":
		y_data = [len(data[data == x_item]) for x_item in x_items]
		return y_data
	else:
		x_item_step = x_items[1] - x_items[0]
		y_data = [len(data[data>=x][data<(x+x_item_step)]) for x in x_items]
		return y_data

def hist2d(x_items,y_items,x_data,y_data):
    if y_items.dtype.kind == "O":
        result = [hist1d(x_items,x_data[y_data == y_item]) for y_item in y_items]
        return result
    else:
        y_item_step = y_items[1] - y_items[0]
        result = [hist1d(x_items,x_data[y_data>=y][y_data<(y+y_item_step)]) for y in y_items]
        return result
