hide_buttons = ["pl_hostname","rowupdate"]

button_strings = {}
for group in nea_config.sections():
    if nea_config.has_option(group,"button_label"):
        button_strings[group] = nea_config.get(group,"button_label")

display_strings = {}
for group in nea_config.sections():
    if nea_config.has_option(group,"axis_label"):
        display_strings[group] = nea_config.get(group,"axis_label")

units = {}
for group in nea_config.sections():
    if nea_config.has_option(group,"units"):
        units[group] = nea_config.get(group,"units")

def get_button_string(s):
    if s in button_strings:
        result = button_strings[s]
    else:
        result = s
    return result

def get_display_string(s, add_units=False):
    if s in display_strings:
        result = display_strings[s]
    else:
        result = s
    if add_units and s in units:
        result += " [%s]" % units[s]
    return result

def get_units(s):
    if s in units:
        result = " %s" % units[s]
    else:
        result = ""
    return result