import math

def bound_round(a1,a2,logfun=False,ignore=False,inclusive=False):
    """Neatly formats two bounds (a1,a2) for graph output. For instance, (2,49) is returned as (0,50).
    
    Keyword arguments:
    logfun -- round only to powers of ten. (2,49) returns as (1,100).
    ignore -- don't perform any bound rounding. (2,49) returns as (2,49).
    inclusive -- rounding up/down is inclusive. (0,10) returns as (-10,20).
    """
    if ignore:
        return (a1,a2)
    if a1 == a2:
        return (a1-1,a2+1)
    if math.floor(a1) == 0 and math.ceil(a2) == 360:
        return (0,360)
    if math.floor(a1) == -90 and math.ceil(a2) == 90:
        return (-90,90)
    if math.floor(a1) == -180 and math.ceil(a2) == 180:
        return (-180,180)
    if logfun:
        if a1 <= 0:
            z1 = a1
        else:
            z1 = 10**floor2(math.log10(a1),inclusive=inclusive)
        if a2 <= 0:
            z2 = a2
        else:
            z2 = 10**ceil2(math.log10(a2),inclusive=inclusive)
        return (z1,z2)
    b = abs(a2 - a1)
    if b >= 1:
        i = 1.0
        while i <= 1e300:
            for j in [1,2,5]:
                c1 = floor2(a1/(i*j),inclusive=inclusive) * i*j
                c2 = ceil2(a2/(i*j),inclusive=inclusive) * i*j
                d = abs(c2 - c1) / (i*j)
                if d <= 6:
                    return (c1,c2)
            i *= 10
    else:
        i = 1.0
        while i >= 1e-300:
            for j in [5,2,1]:
                c1 = floor2(a1/(i*j),inclusive=inclusive) * i*j
                c2 = ceil2(a2/(i*j),inclusive=inclusive) * i*j
                d = abs(c2 - c1) / (i*j)
                if d > 6:
                    return (c1,c2)
            i /= 10
    return (a1,a2)

def floor2(x,inclusive=False):
    """Allows inclusive rounding down of integers. If inclusive is true, 2 rounds down to 1"""
    if x == math.floor(x) and not inclusive:
        return x - 1
    else:
        return math.floor(x)

def ceil2(x,inclusive=False):
    """Allows inclusive rounding up of integers. If inclusive is true, 2 rounds up to 3"""
    if x == math.ceil(x) and not inclusive:
        return x + 1
    else:
        return math.ceil(x)