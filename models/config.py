# -*- coding: utf-8 -*-

import os, ConfigParser
config = ConfigParser.RawConfigParser()
config.read(os.path.join(request.env.gluon_parent,"filtergraph.cfg"))

#http://stackoverflow.com/questions/4028904/how-to-get-the-home-directory-in-python

FG_DATA_FOLDER = config.get("path","data")
FG_SAVESHARE_FOLDER = os.path.join(FG_DATA_FOLDER,"saveshare")
try:
    NEA_DATA_FILE = config.get("nea","file")
except:
    NEA_DATA_FILE = os.path.join(request.folder,"static","planets.npy")
try:
    NEA_CONFIG_FILE = config.get("nea","config_file")
except:
    NEA_CONFIG_FILE = os.path.join(request.folder,"static","planets.cfg")

nea_config = ConfigParser.RawConfigParser()
nea_config.read(NEA_CONFIG_FILE)